# syntax=docker/dockerfile:1

FROM golang:1.20.2-alpine3.17 as BUILD
WORKDIR /app

COPY . /app
RUN go mod download && go mod tidy && go mod vendor
RUN go build -o helloworld .
RUN mkdir build && \ 
    cp -p helloworld build

# PROCESS MULTI STAGE
FROM scratch
WORKDIR /app
COPY --from=BUILD /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=BUILD /app/build /app
CMD ["./helloworld"]
